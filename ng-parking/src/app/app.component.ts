import { Component } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import 'rxjs/add/observable/timer';

import { SimpleParkingSystem } from './auto-parking/implementation/simple-parking-system';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';

class CarStatus {
  id: string;
  transform: string;
  opacity: number;
  backColor: string;
  transition: number;
  x: number;
  y: number;
}

declare var window: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'Automatic Parking System';

  parking = new SimpleParkingSystem(15, 15);

  cols = new Array(15).fill(0).map((x, i) => i + 1);

  rows = new Array(15).fill(0).map((x, i) => i + 1).reverse();

  cars: Array<CarStatus> = [];

  timerSub: Subscription = null;

  commands = '5,5:FLFLFFRFFF';

  animationSpeed = 800;

  playingLength = 0;

  playingStep = 0;

  lastPosition = '';

  hasError = false;

  example1() {
    this.commands = '5,5:RFLFRFLF';
    this.commandsChanged();
  }

  example2() {
    this.commands = '6,6:FFLFFLFFLFF';
    this.commandsChanged();
  }

  example3() {
    this.commands = '5,5:FLFLFFRFFF';
    this.commandsChanged();
  }

  longTripClockwise() {
    this.setCommands('1,1:FFFFFFFFFFFFFFRFFFFFFFFFFFFFFRFFFFFFFFFFFFFFRFFFFFFFFFFFF');
  }

  longTripCounterclockwise() {
    this.setCommands('1,1:RFFFFFFFFFFFFFFLFFFFFFFFFFFFFFLFFFFFFFFFFFFFFLFFFFFFFFFFFF');
  }

  longTripTopRightCorner() {
    this.setCommands('1,1:FRFLFRFLFRFLFRFLFRFLFRFLFRFLFRFLFRFLFRFLFRFLFRFLFRFLFRF');
  }

  setCommands(input: string) {
    this.commands = input;
    this.commandsChanged();
  }

  ngOnInit(): void {
    this.playingStep = 0;
    this.stopCommands();
  }

  trackById(index, item) {
    return item.id;
  }

  playNextStep(step: number) {
    const carSteps = [];
    for (let i = 0; i <= step; i++) {
      this.parking.prepare(this.commands);
      this.playingLength = this.parking.getCommandCount();

      this.parking.executePartially(i);
      this.hasError = this.hasError || this.parking.hasCommandError();

      const carX = this.parking.getCarPositionX();
      const carY = this.parking.getCarPositionY();

      const car = new CarStatus();
      car.id = 'car_' + i;

      car.y = (15 - carX) * 46;
      car.x = (carY - 1) * 46;

      const deg = this.parking.getCarRotation();
      car.transform = 'rotate(' + deg + 'deg)';
      car.backColor = '';

      // Short delay after the animation
      car.transition = this.animationSpeed * 0.7 / 1000.0;

      car.opacity = 0.3;
      if (i === step) {
        car.opacity = 1.0;
        car.id = 'last';
      } else if (i === 0) {
        car.backColor = 'black';
      }

      carSteps.push(car);
    }

    return carSteps;
  }

  commandsChanged() {
    try {
      this.hasError = false;
      this.playingStep = 0;
      this.cars = this.playNextStep(0);
      this.lastPosition = this.parking.getCarPositionString();
    } catch (err) {
      this.hasError = true;
    }
  }

  stepCommands() {
    try {
      this.playingStep++;
      if (this.playingStep >= this.playingLength) {
        if (this.timerSub) {
          this.timerSub.unsubscribe();
          this.timerSub = null;
        }
      }

      this.cars = this.playNextStep(this.playingStep);
      this.lastPosition = this.parking.getCarPositionString();
    } catch (err) {
      this.hasError = true;
    }
  }

  playCommands() {
    const period = this.animationSpeed;
    this.timerSub = Observable.timer(500, period).subscribe(() => {
      this.stepCommands();
    });
  }

  stopCommands() {
    this.playingStep = 0;
    this.cars = this.playNextStep(0);
    this.lastPosition = this.parking.getCarPositionString();

    if (this.timerSub) {
      this.timerSub.unsubscribe();
      this.timerSub = null;
    }
  }

}
