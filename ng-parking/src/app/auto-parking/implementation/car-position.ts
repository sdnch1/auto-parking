export class CarPosition {

  private valueX: number;
  private valueY: number;

  constructor(x: number, y: number) {
    this.valueX = x;
    this.valueY = y;
  }

  get x() {
    return this.valueX;
  }

  get y() {
    return this.valueY;
  }

}
