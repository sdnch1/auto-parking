import { CarDirection } from '../parking-system-inspect';

export class CarRotation {

  private directionValue: CarDirection;
  private rotationValue: number;

  constructor(direction: CarDirection, rotation: number) {
    this.directionValue = direction;
    this.rotationValue = rotation;
  }

  get direction() {
    return this.directionValue;
  }

  get rotation() {
    return this.rotationValue;
  }

}
