import { ParkingSystemInspection } from '../parking-system-inspect';

import { POSITION_INITIAL } from '../parking-system-inspect';
import { RESULT_SUCCESS } from '../parking-system-inspect';
import { CarDirection } from '../parking-system-inspect';
import { CarPosition } from './car-position';
import { CarRotation } from './car-rotation';

export class SimpleParkingSystem implements ParkingSystemInspection {


  private parkWidth: number;

  private parkHeight: number;

  private carPosition = new CarPosition(POSITION_INITIAL, POSITION_INITIAL);

  private carDirection = CarDirection.North;

  private carRotation = 0;

  /**
  * Indicates which command causes the error.
  */
  private commandErrorPos = RESULT_SUCCESS;

  private commandActions = '';

  constructor(width: number, height: number) {
    this.parkWidth = width;
    this.parkHeight = height;
  }

  getCarPositionX(): number {
    return this.carPosition.x;
  }

  getCarPositionY(): number {
    return this.carPosition.y;
  }

  getCarDirection(): number {
    return this.carDirection;
  }

  getCarRotation(): number {
    return this.carRotation;
  }

  getCommandErrorPos(): number {
    return this.commandErrorPos;
  }

  hasCommandError(): boolean {
    return this.commandErrorPos >= 0;
  }

  getCarPositionString(): string {
    return this.carPosition.x + ',' + this.carPosition.y;
  }

  getCommandCount(): number {
    return this.commandActions.length;
  }

  private checkIfExceeding(p: CarPosition): boolean {
    if (p.x < 1 || p.x > this.parkWidth) {
      return true;
    }

    if (p.y < 1 || p.y > this.parkHeight) {
      return true;
    }

    return false;
  }

  private setFirstErrorPos(pos: number): void {
    if (this.commandErrorPos === RESULT_SUCCESS) {
      this.commandErrorPos = pos;
    }
  }

  prepare(input: string): void {
    this.commandErrorPos = RESULT_SUCCESS;

    let positionString = '';
    let instructions = '';
    const parts = input.split(':', 2);
    if (parts.length === 2) {
      positionString = parts[0];
      instructions = parts[1];
    } else {
      throw new Error('Wrong commands');
    }

    let initialX = POSITION_INITIAL;
    let initialY = POSITION_INITIAL;
    const positionParts = positionString.split(',', 2);
    if (positionParts.length === 2) {
      initialX = Number.parseInt(positionParts[0]);
      initialY = Number.parseInt(positionParts[1]);
    } else {
      throw new Error('Wrong initial position');
    }

    this.carPosition = new CarPosition(initialX, initialY);
    this.carDirection = CarDirection.North;
    this.commandActions = instructions;
    this.carRotation = 0;

    if (this.checkIfExceeding(this.carPosition)) {
      this.setFirstErrorPos(0);
      throw new Error('Wrong initial position');
    }
  }

  private turnLeft(r: CarRotation): CarRotation {
    let newDirection = r.direction;
    const newRotation = r.rotation - 90;

    switch (r.direction) {
      case CarDirection.North:
        newDirection = CarDirection.West;
        break;
      case CarDirection.West:
        newDirection = CarDirection.South;
        break;
      case CarDirection.South:
        newDirection = CarDirection.East;
        break;
      case CarDirection.East:
        newDirection = CarDirection.North;
        break;
      default:
        throw new Error('Wrong direction');
    }

    return new CarRotation(newDirection, newRotation);
  }

  private turnRight(r: CarRotation): CarRotation {
    let newDirection = r.direction;
    const newRotation = r.rotation + 90;

    switch (r.direction) {
      case CarDirection.North:
        newDirection = CarDirection.East;
        break;
      case CarDirection.West:
        newDirection = CarDirection.North;
        break;
      case CarDirection.South:
        newDirection = CarDirection.West;
        break;
      case CarDirection.East:
        newDirection = CarDirection.South;
        break;
      default:
        throw new Error('Wrong direction');
    }

    return new CarRotation(newDirection, newRotation);
  }

  private moveForword(direction: CarDirection, location: CarPosition): CarPosition {
    let x = location.x;
    let y = location.y;

    switch (direction) {
      case CarDirection.North:
        x++;
        break;
      case CarDirection.West:
        y--;
        break;
      case CarDirection.South:
        x--;
        break;
      case CarDirection.East:
        y++;
        break;
      default:
        throw new Error('Wrong direction');
    }

    return new CarPosition(x, y);
  }

  executePartially(count: number): void {
    const commandCountToRun = Math.min(count, this.commandActions.length);
    for (let i = 0; i < commandCountToRun; i++) {
      let newPosition = this.carPosition;
      let rotation = new CarRotation(this.carDirection, this.carRotation);

      const command = this.commandActions.charAt(i);
      switch (command) {
        case 'L':
          rotation = this.turnLeft(rotation);
          this.carDirection = rotation.direction;
          this.carRotation = rotation.rotation;
          break;
        case 'R':
          rotation = this.turnRight(rotation);
          this.carDirection = rotation.direction;
          this.carRotation = rotation.rotation;
          break;
        case 'F':
          newPosition = this.moveForword(this.carDirection, newPosition);
          break;
        default:
          this.setFirstErrorPos(i + 1);
          throw new Error('Wrong command');
      }

      if (this.checkIfExceeding(newPosition)) {
        // Only save the first error position, ignore the command
        this.setFirstErrorPos(i + 1);
      } else {
        this.carPosition = newPosition;
      }
    }
  }

  execute(commands: string) {
    this.prepare(commands);
    this.executePartially(this.getCommandCount());
  }

}
