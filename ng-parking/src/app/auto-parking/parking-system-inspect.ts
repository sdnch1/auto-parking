import { ParkingSystem } from "./parking-system";

export enum CarDirection {
  North = 1,
  South = 2,
  West = 3,
  East = 4,
}

export const RESULT_SUCCESS = -1;

export const POSITION_INITIAL = -1;

export interface ParkingSystemInspection extends ParkingSystem {

  getCarPositionX(): number;

  getCarPositionY(): number;

  getCarDirection(): number;

  getCarRotation(): number;

  getCommandErrorPos(): number;

  prepare(input: string): void;

  getCommandCount(): number;

  executePartially(count: number): void;

}
