export interface ParkingSystem {

  getCarPositionString(): string;

  execute(commands: string);

  hasCommandError(): boolean;

}
