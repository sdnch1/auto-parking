# 0. Question

## Automatic Parking System Problem

The purpose of this exercise is to demonstrate TDD and come up with a solution to the problem given below. 
Working unit tests will be sufficient as a solution, but you may provide a simple user interface 
to demonstrate your codes capability if you desire.

Cars are placed on a 15 by 15 grid at particular co-ordinates heading north, and the simple commands Left, 
right and forward are transmitted to them. The commands must be executed and the final position calculated.

## The following examples should be used to demonstrate your code:

* For the input **"5,5:RFLFRFLF"** - *We should get the position "7,7"*
* For the input **"6,6:FFLFFLFFLFF"** - *We should get the position "6,6"*
* For the input **"5,5:FLFLFFRFFF"** - *We should get the position "4,1"*

> You can either zip your code up and email it to us, or provide a link to the project online.

# 1. Solution

## Additional Assumptions
 
Because the requirements are quite simple and vague, suppose the question is
a simple one. (I could be wrong, but have no sufficient time to confirm via email 
and complete the exercise, the question has more problems than the first look.)

I add more assumptions as the supplement of the original requirements.
* There is only one car at one time. No collisions with other cars to be
considered.
* Movements over the car park boundary will be treated as invalid, and
will be ignored. As this is not a part of the original question, the unit
tests only detect invalid commands, instead of getting the final position.

## Find Appropriate Coordinate Frame

The question does not give the direction of x-axis and y-axis. We can
simulate the movements by hand using the third test case (5,5:FLFLFFRFFF). 
Because, in the other two test cases, x-axis and y-axis are symmetrical.

Imagine we are on a map, the north is on the top side, and the west is on the
left side. After executing the commands, the relative position is +4 in west,
and +1 in south direction.

The result given by the test case is (4,1). The y-coordinate decreases by 4,
so the west is the negative direction of y-axis. The x-coordinate decreases
by 1, so the south is the negative direction of x-axis.

In conclusion, x-axis increases from south to north, and y-axis increases
from west to east. The original point of the coordinate frame is on the
left-bottom corner. The x-axis is vertical and y-axis is horizontal.

# 2. User Interface for Demonstration

More internal details are required to render the car on the map, like car position, 
direction and even the rotation angle of the car (for animations).
Another java interface "ParkingSystemInspection" is added to expose more internal states.

The UI program is written in Angular (TypeScript), and the solution codes are literally 
translated into TypeScript, except the unit testing codes.

All the unit tests are in the Java project.

# 3. How to Use

## Java Code

It's a `Maven` project, you should have `JDK 1.8` installed.

To run the test cases, extract the zip file, and go into the project folder, run the command:

```
mvnw test
```

## Angular Code

The application has been built, and the compiled code is in folder `ng-parking-dist`,
which is located in the Java project folder.

Open the file `index.html` (inside of `ng-parking-dist`) with Chrome browser.
(Not tested in other browsers)

### User Guide for the Angular Application

* Change the commands in the text box, and press "Play" to see how the car moves.
* You can also press "Step" instead of "Play" to see the steps more clearly.
* The button "Stop" is used to reset the car posture to initial state.
* Some predefined examples of the commands can be chosen by other buttons.

# 4. Screenshots
![See the screenshot for the UI example](auto-parking-demo.png)
