package com.xychar.parking;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.xychar.parking.impl.SimpleParkingSystem;

public class ParkingSystemBasicTest {

	private ParkingSystem parkingSystem;

	@Before
	public void setup() {
		parkingSystem = new SimpleParkingSystem(15, 15);
	}

	@Test
	public void testCase1() {
		parkingSystem.execute("5,5:RFLFRFLF");
		Assert.assertEquals("7,7", parkingSystem.getCarPositionString());
	}

	@Test
	public void testCase2() {
		parkingSystem.execute("6,6:FFLFFLFFLFF");
		Assert.assertEquals("6,6", parkingSystem.getCarPositionString());
	}

	@Test
	public void testCase3() {
		parkingSystem.execute("5,5:FLFLFFRFFF");
		Assert.assertEquals("4,1", parkingSystem.getCarPositionString());
	}

}