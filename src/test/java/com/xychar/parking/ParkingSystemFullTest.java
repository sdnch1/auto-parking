package com.xychar.parking;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.xychar.parking.impl.SimpleParkingSystem;

public class ParkingSystemFullTest {

	private ParkingSystem parkingSystem;

	private static String duplicateString(String str, int count) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < count; i++) {
			sb.append(str);
		}

		return sb.toString();
	}

	@Before
	public void setup() {
		parkingSystem = new SimpleParkingSystem(15, 15);
	}

	@Test
	public void testNoCommands() {
		parkingSystem.execute("1,1:");
		Assert.assertEquals("1,1", parkingSystem.getCarPositionString());

		parkingSystem.execute("15,15:");
		Assert.assertEquals("15,15", parkingSystem.getCarPositionString());

		parkingSystem.execute("1,15:");
		Assert.assertEquals("1,15", parkingSystem.getCarPositionString());

		parkingSystem.execute("15,1:");
		Assert.assertEquals("15,1", parkingSystem.getCarPositionString());
	}

	@Test
	public void testNoCommandsWithWrongPosition() {
		parkingSystem.execute("0,0:");
		Assert.assertTrue(parkingSystem.hasCommandError());

		parkingSystem.execute("16,16:");
		Assert.assertTrue(parkingSystem.hasCommandError());

		parkingSystem.execute("0,1:");
		Assert.assertTrue(parkingSystem.hasCommandError());

		parkingSystem.execute("0,15:");
		Assert.assertTrue(parkingSystem.hasCommandError());
	}

	@Test(expected = InvalidCommandException.class)
	public void testInvalidCommandsFormatEmptyInput() {
		parkingSystem.execute("");
	}

	@Test(expected = InvalidCommandException.class)
	public void testInvalidCommandsFormatNoInitialPostion() {
		parkingSystem.execute(":");
	}

	@Test(expected = NumberFormatException.class)
	public void testInvalidCommandsFormatNotInteger() {
		parkingSystem.execute("L,1:");
	}

	@Test(expected = InvalidCommandException.class)
	public void testInvalidCommandsFormatNoColon() {
		parkingSystem.execute("0,1");
	}

	@Test(expected = InvalidCommandException.class)
	public void testInvalidCommandsFormatNoComma() {
		parkingSystem.execute("16:");
	}

	@Test
	public void testTurningLeft() {
		parkingSystem.execute("7,7:");
		Assert.assertEquals("7,7", parkingSystem.getCarPositionString());

		parkingSystem.execute("7,7:F");
		Assert.assertEquals("8,7", parkingSystem.getCarPositionString());

		parkingSystem.execute("7,7:LF");
		Assert.assertEquals("7,6", parkingSystem.getCarPositionString());

		parkingSystem.execute("7,7:LLF");
		Assert.assertEquals("6,7", parkingSystem.getCarPositionString());

		parkingSystem.execute("7,7:LLLF");
		Assert.assertEquals("7,8", parkingSystem.getCarPositionString());

		parkingSystem.execute("7,7:LLLLF");
		Assert.assertEquals("8,7", parkingSystem.getCarPositionString());
	}

	@Test
	public void testTurningRight() {
		parkingSystem.execute("7,7:");
		Assert.assertEquals("7,7", parkingSystem.getCarPositionString());

		parkingSystem.execute("7,7:F");
		Assert.assertEquals("8,7", parkingSystem.getCarPositionString());

		parkingSystem.execute("7,7:RF");
		Assert.assertEquals("7,8", parkingSystem.getCarPositionString());

		parkingSystem.execute("7,7:RRF");
		Assert.assertEquals("6,7", parkingSystem.getCarPositionString());

		parkingSystem.execute("7,7:RRRF");
		Assert.assertEquals("7,6", parkingSystem.getCarPositionString());

		parkingSystem.execute("7,7:RRRRF");
		Assert.assertEquals("8,7", parkingSystem.getCarPositionString());
	}

	@Test
	public void testMovingForward() {
		parkingSystem.execute("7,7:");
		Assert.assertEquals("7,7", parkingSystem.getCarPositionString());

		parkingSystem.execute("7,7:F");
		Assert.assertEquals("8,7", parkingSystem.getCarPositionString());

		parkingSystem.execute("7,7:FF");
		Assert.assertEquals("9,7", parkingSystem.getCarPositionString());

		parkingSystem.execute("7,7:FFF");
		Assert.assertEquals("10,7", parkingSystem.getCarPositionString());

		parkingSystem.execute("5,5:FFFFFFFFFF");
		Assert.assertEquals("15,5", parkingSystem.getCarPositionString());

		parkingSystem.execute("7,7:RFFFFFF");
		Assert.assertEquals("7,13", parkingSystem.getCarPositionString());

		parkingSystem.execute("7,7:RRFFFFFF");
		Assert.assertEquals("1,7", parkingSystem.getCarPositionString());

		parkingSystem.execute("7,7:RRRFFFFFF");
		Assert.assertEquals("7,1", parkingSystem.getCarPositionString());

		parkingSystem.execute("7,7:RRRRFFFFFF");
		Assert.assertEquals("13,7", parkingSystem.getCarPositionString());
	}

	@Test(expected = InvalidCommandException.class)
	public void testWrongCommandActions1() {
		parkingSystem.execute("6,6:M");
	}

	@Test(expected = InvalidCommandException.class)
	public void testWrongCommandActions2() {
		parkingSystem.execute("6,6:_");
	}

	@Test(expected = InvalidCommandException.class)
	public void testWrongCommandActions3() {
		parkingSystem.execute("8,8:5");
	}

	@Test
	public void testLongJourneyClockwise() {
		StringBuilder sb = new StringBuilder();

		sb.append("1,1:").append(duplicateString("F", 14));
		sb.append("R").append(duplicateString("F", 14));
		sb.append("R").append(duplicateString("F", 14));
		sb.append("R").append(duplicateString("F", 14));

		parkingSystem.execute(sb.toString());
		Assert.assertEquals("1,1", parkingSystem.getCarPositionString());
	}

	@Test
	public void testLongJourneyCounterclockwise() {
		StringBuilder sb = new StringBuilder();

		sb.append("1,1:R").append(duplicateString("F", 14));
		sb.append("L").append(duplicateString("F", 14));
		sb.append("L").append(duplicateString("F", 14));
		sb.append("L").append(duplicateString("F", 14));

		parkingSystem.execute(sb.toString());
		Assert.assertEquals("1,1", parkingSystem.getCarPositionString());
	}

	@Test
	public void testMoveExceedingBoundary() {
		parkingSystem.execute("1,15:" + duplicateString("F", 15));
		Assert.assertTrue(parkingSystem.hasCommandError());

		parkingSystem.execute("1,15:RF");
		Assert.assertTrue(parkingSystem.hasCommandError());

		parkingSystem.execute("15,15:F");
		Assert.assertTrue(parkingSystem.hasCommandError());

		parkingSystem.execute("15,15:RF");
		Assert.assertTrue(parkingSystem.hasCommandError());

		parkingSystem.execute("1,1:LF");
		Assert.assertTrue(parkingSystem.hasCommandError());

		parkingSystem.execute("1,1:RRF");
		Assert.assertTrue(parkingSystem.hasCommandError());
	}

}