package com.xychar.parking;

/**
 * This interface expose more details, for GUI rendering.
 */
public interface ParkingSystemInspection extends ParkingSystem {

	int RESULT_SUCCESS = -1;

	int POSITION_INITIAL = -1;

	int CAR_DIRECTION_NORTH = 1;

	int CAR_DIRECTION_SOUTH = 2;

	int CAR_DIRECTION_WEST = 3;

	int CAR_DIRECTION_EAST = 4;

	int getCarPositionX();

	int getCarPositionY();

	int getCarDirection();

	int getCommandErrorPos();

	void prepare(String input);

	int getCommandCount();

	void executePartially(int count);

}
