package com.xychar.parking;

/**
 * Automatic Parking system.
 * <p>
 * Given instructions, the system will move the car to the right place.
 * <p>
 */
public interface ParkingSystem {

	String getCarPositionString();

	void execute(String commands);

	boolean hasCommandError();

}
