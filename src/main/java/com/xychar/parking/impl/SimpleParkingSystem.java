package com.xychar.parking.impl;

import com.xychar.parking.InvalidCommandException;
import com.xychar.parking.ParkingSystemInspection;

/**
 * A simple implementation of interface ParkingSystem.
 */
public class SimpleParkingSystem implements ParkingSystemInspection {

	private final int parkWidth;

	private final int parkHeight;

	private CarPosition carPosition = new CarPosition(POSITION_INITIAL, POSITION_INITIAL);

	private int carDirection = CAR_DIRECTION_NORTH;

	/**
	 * Indicates which command causes the error.
	 */
	private int commandErrorPos = RESULT_SUCCESS;

	private String commandActions = "";

	public SimpleParkingSystem(int width, int height) {
		this.parkWidth = width;
		this.parkHeight = height;
	}

	public int getCarPositionX() {
		return carPosition.getX();
	}

	public int getCarPositionY() {
		return carPosition.getY();
	}

	@Override
	public int getCarDirection() {
		return carDirection;
	}

	public int getCommandErrorPos() {
		return commandErrorPos;
	}

	@Override
	public boolean hasCommandError() {
		return commandErrorPos >= 0;
	}

	@Override
	public String getCarPositionString() {
		return carPosition.getX() + "," + carPosition.getY();
	}

	public int getCommandCount() {
		return commandActions.length();
	}

	private boolean checkIfExceeding(CarPosition p) {
		if (p.getX() < 1 || p.getX() > parkWidth) {
			return true;
		}

		if (p.getY() < 1 || p.getY() > parkHeight) {
			return true;
		}

		return false;
	}

	/**
	 * Only save the first error position
	 */
	private void setFirstErrorPos(int pos) {
		if (commandErrorPos == RESULT_SUCCESS) {
			commandErrorPos = pos;
		}
	}

	public void prepare(String input) {
		this.commandErrorPos = RESULT_SUCCESS;

		String positionString = "";
		String instructions = "";
		String[] parts = input.split(":", 2);
		if (parts.length == 2) {
			positionString = parts[0];
			instructions = parts[1];
		} else {
			throw new InvalidCommandException("Wrong commands");
		}

		int initialX = POSITION_INITIAL;
		int initialY = POSITION_INITIAL;
		String[] positionParts = positionString.split(",", 2);
		if (positionParts.length == 2) {
			initialX = Integer.parseInt(positionParts[0]);
			initialY = Integer.parseInt(positionParts[1]);
		} else {
			throw new InvalidCommandException("Wrong initial position");
		}

		this.carPosition = new CarPosition(initialX, initialY);
		this.carDirection = CAR_DIRECTION_NORTH;
		this.commandActions = instructions;

		if (checkIfExceeding(this.carPosition)) {
			setFirstErrorPos(0);
		}
	}

	private int turnLeft(int direction) {
		switch (direction) {
		case CAR_DIRECTION_NORTH:
			return CAR_DIRECTION_WEST;
		case CAR_DIRECTION_WEST:
			return CAR_DIRECTION_SOUTH;
		case CAR_DIRECTION_SOUTH:
			return CAR_DIRECTION_EAST;
		case CAR_DIRECTION_EAST:
			return CAR_DIRECTION_NORTH;
		default:
			throw new IllegalStateException("Wrong direction");
		}
	}

	private int turnRight(int direction) {
		switch (direction) {
		case CAR_DIRECTION_NORTH:
			return CAR_DIRECTION_EAST;
		case CAR_DIRECTION_WEST:
			return CAR_DIRECTION_NORTH;
		case CAR_DIRECTION_SOUTH:
			return CAR_DIRECTION_WEST;
		case CAR_DIRECTION_EAST:
			return CAR_DIRECTION_SOUTH;
		default:
			throw new IllegalStateException("Wrong direction");
		}
	}

	private CarPosition moveForword(int direction, CarPosition location) {
		int x = location.getX();
		int y = location.getY();

		switch (direction) {
		case CAR_DIRECTION_NORTH:
			x++;
			break;
		case CAR_DIRECTION_WEST:
			y--;
			break;
		case CAR_DIRECTION_SOUTH:
			x--;
			break;
		case CAR_DIRECTION_EAST:
			y++;
			break;
		default:
			throw new IllegalStateException("Wrong direction");
		}

		return new CarPosition(x, y);
	}

	public void executePartially(int count) {
		int commandCountToRun = Math.min(count, commandActions.length());
		for (int i = 0; i < commandCountToRun; i++) {
			CarPosition newPosition = carPosition;

			char command = commandActions.charAt(i);
			switch (command) {
			case 'L':
				carDirection = turnLeft(carDirection);
				break;
			case 'R':
				carDirection = turnRight(carDirection);
				break;
			case 'F':
				newPosition = moveForword(carDirection, newPosition);
				break;
			default:
				setFirstErrorPos(i + 1);
				throw new InvalidCommandException("Wrong command");
			}

			if (checkIfExceeding(newPosition)) {
				// Only save the first error position, ignore the command
				setFirstErrorPos(i + 1);
			} else {
				carPosition = newPosition;
			}
		}
	}

	public void execute(String commands) {
		prepare(commands);
		executePartially(getCommandCount());
	}

}
