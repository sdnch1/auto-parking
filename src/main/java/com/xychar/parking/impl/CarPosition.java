package com.xychar.parking.impl;

public class CarPosition {

	private final int x;

	private final int y;

	public CarPosition(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

}
