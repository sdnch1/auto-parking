webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

var escape = __webpack_require__("../../../../css-loader/lib/url/escape.js");
exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "div.ap-main {\r\n    text-align: center\r\n}\r\n\r\ndiv.ap-center {\r\n    display: inline-block;\r\n}\r\n\r\ntable.ap-park {\r\n    border: 1px solid lightgray;\r\n    border-collapse: collapse;\r\n    table-layout: fixed;\r\n    width: 676px;\r\n}\r\n\r\ntd.ap-park-cell {\r\n    border: 1px solid lightgray;\r\n    text-align: center;\r\n    width: 46px;\r\n    height: 46px;\r\n    padding: 0px;\r\n    color: darkgray;\r\n}\r\n\r\ndiv.ap-form {\r\n    width: 320px;\r\n    min-height: 500px;\r\n    border: 1px solid darkgray;\r\n    border-radius: 5px;\r\n    margin: 15px 15px;\r\n    float: left;\r\n    text-align: left;\r\n    padding: 15px;\r\n}\r\n\r\ndiv.btn-more-example {\r\n    margin: 15px 0px;\r\n}\r\n\r\ndiv.ap-board {\r\n    border: 1px solid darkgray;\r\n    position: relative;\r\n    float: left;\r\n}\r\n\r\ndiv.ap-car-plane {\r\n    position: absolute;\r\n    left: 1px;\r\n    top: 1px;\r\n    width: 0px;\r\n    height: 0px;\r\n}\r\n\r\ndiv.ap-car-taxi {\r\n    position: absolute;\r\n    width: 45px;\r\n    height: 45px;\r\n    background-image: url(" + escape(__webpack_require__("../../../../../src/assets/taxi.png")) + ");\r\n    background-size: 40px 40px;\r\n    background-repeat: no-repeat;\r\n    background-position: center;\r\n    -webkit-transition: 0.4s;\r\n    transition: 0.4s;\r\n}\r\n\r\ndiv.ap-car-taxi.car-dir-north {\r\n    -webkit-transform: rotate(90deg);\r\n            transform: rotate(90deg);\r\n}\r\n\r\ndiv.ap-car-taxi.car-dir-south {\r\n    -webkit-transform: rotate(190deg);\r\n            transform: rotate(190deg);\r\n}\r\n\r\ndiv.ap-car-taxi.car-dir-west {\r\n    -webkit-transform: rotate(90deg);\r\n            transform: rotate(90deg);\r\n}\r\n\r\ndiv.ap-car-taxi.car-dir-north {\r\n    -webkit-transform: rotate(90deg);\r\n            transform: rotate(90deg);\r\n}\r\n\r\ndiv.ap-form div.form-group label {\r\n    width: 100%;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<div style=\"text-align:center\">\n  <h1>\n    Automatic Parking System\n  </h1>\n</div>\n<div class=\"ap-main\">\n  <div class=\"ap-center\">\n    <div class=\"ap-form\">\n      <div class=\"form-group\">\n        <div class=\"alert alert-info\"\n             [hidden]=\"hasError\">\n          <strong>Position</strong>: ({{lastPosition}})\n        </div>\n        <div class=\"alert alert-danger\"\n             [hidden]=\"!hasError\">\n          Errors in commands!\n        </div>\n      </div>\n\n      <div class=\"form-group\">\n        <label>Commands:\n          <input class=\"form-control\"\n                 type=\"text\"\n                 [(ngModel)]=\"commands\"\n                 (ngModelChange)=\"commandsChanged()\">\n        </label>\n      </div>\n\n      <div class=\"form-group\">\n        <label>Animations:\n          <div class=\"radio\">\n            <label>\n              <input type=\"radio\"\n                     name=\"speed\"\n                     [disabled]=\"!!timerSub\"\n                     [value]=\"800\"\n                     [(ngModel)]=\"animationSpeed\"> Slow</label>\n          </div>\n          <div class=\"radio\">\n            <label>\n              <input type=\"radio\"\n                     name=\"speed\"\n                     [disabled]=\"!!timerSub\"\n                     [value]=\"300\"\n                     [(ngModel)]=\"animationSpeed\"> Fast</label>\n          </div>\n        </label>\n      </div>\n\n      <div class=\"form-group\">\n        <div class=\"btn-group\">\n          <button class=\"btn btn-default\"\n                  [disabled]=\"!!timerSub\"\n                  (click)=\"playCommands()\">Play</button>\n          <button class=\"btn btn-default\"\n                  (click)=\"stepCommands()\">Step</button>\n          <button class=\"btn btn-default\"\n                  (click)=\"stopCommands()\">Stop</button>\n        </div>\n      </div>\n\n      <div class=\"form-group\">\n        <span>Examples:</span>\n        <div class=\"btn-more-example\">\n          <div class=\"btn-group\">\n            <button class=\"btn btn-default\"\n                    (click)=\"example1()\">Example1</button>\n            <button class=\"btn btn-default\"\n                    (click)=\"example2()\">Example2</button>\n            <button class=\"btn btn-default\"\n                    (click)=\"example3()\">Example3</button>\n          </div>\n        </div>\n      </div>\n\n      <div class=\"form-group\">\n        <span>More Examples:</span>\n        <div class=\"btn-more-example\">\n          <button class=\"btn btn-default\"\n                  (click)=\"longTripClockwise()\">Long Trip Clockwise</button>\n        </div>\n        <div class=\"btn-more-example\">\n          <button class=\"btn btn-default\"\n                  (click)=\"longTripCounterclockwise()\">Long Trip Counterclockwise</button>\n        </div>\n        <div class=\"btn-more-example\">\n          <button class=\"btn btn-default\"\n                  (click)=\"longTripTopRightCorner()\">Long Trip to Top Right</button>\n        </div>\n      </div>\n    </div>\n\n    <div class=\"ap-board\">\n      <table class=\"ap-park\">\n        <tbody>\n          <tr class=\"ap-park-row\"\n              *ngFor=\"let row of rows\">\n            <td class=\"ap-park-cell\"\n                *ngFor=\"let col of cols\">\n              {{row}},{{col}}\n            </td>\n          </tr>\n        </tbody>\n      </table>\n      <div class=\"ap-car-plane\">\n        <div class=\"ap-car-taxi\"\n             *ngFor=\"let car of cars; trackBy: trackById\"\n             id=\"{{car.id}}\"\n             [style.left.px]=\"car.x\"\n             [style.top.px]=\"car.y\"\n             [style.transform]=\"car.transform\"\n             [style.opacity]=\"car.opacity\"\n             [style.backgroundColor]=\"car.backColor\"\n             [style.transition.s]=\"car.transition\"></div>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__ = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_observable_timer__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/timer.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__auto_parking_implementation_simple_parking_system__ = __webpack_require__("../../../../../src/app/auto-parking/implementation/simple-parking-system.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var CarStatus = /** @class */ (function () {
    function CarStatus() {
    }
    return CarStatus;
}());
var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'Automatic Parking System';
        this.parking = new __WEBPACK_IMPORTED_MODULE_3__auto_parking_implementation_simple_parking_system__["a" /* SimpleParkingSystem */](15, 15);
        this.cols = new Array(15).fill(0).map(function (x, i) { return i + 1; });
        this.rows = new Array(15).fill(0).map(function (x, i) { return i + 1; }).reverse();
        this.cars = [];
        this.timerSub = null;
        this.commands = '5,5:FLFLFFRFFF';
        this.animationSpeed = 800;
        this.playingLength = 0;
        this.playingStep = 0;
        this.lastPosition = '';
        this.hasError = false;
    }
    AppComponent.prototype.example1 = function () {
        this.commands = '5,5:RFLFRFLF';
        this.commandsChanged();
    };
    AppComponent.prototype.example2 = function () {
        this.commands = '6,6:FFLFFLFFLFF';
        this.commandsChanged();
    };
    AppComponent.prototype.example3 = function () {
        this.commands = '5,5:FLFLFFRFFF';
        this.commandsChanged();
    };
    AppComponent.prototype.longTripClockwise = function () {
        this.setCommands('1,1:FFFFFFFFFFFFFFRFFFFFFFFFFFFFFRFFFFFFFFFFFFFFRFFFFFFFFFFFF');
    };
    AppComponent.prototype.longTripCounterclockwise = function () {
        this.setCommands('1,1:RFFFFFFFFFFFFFFLFFFFFFFFFFFFFFLFFFFFFFFFFFFFFLFFFFFFFFFFFF');
    };
    AppComponent.prototype.longTripTopRightCorner = function () {
        this.setCommands('1,1:FRFLFRFLFRFLFRFLFRFLFRFLFRFLFRFLFRFLFRFLFRFLFRFLFRFLFRF');
    };
    AppComponent.prototype.setCommands = function (input) {
        this.commands = input;
        this.commandsChanged();
    };
    AppComponent.prototype.ngOnInit = function () {
        this.playingStep = 0;
        this.stopCommands();
    };
    AppComponent.prototype.trackById = function (index, item) {
        return item.id;
    };
    AppComponent.prototype.playNextStep = function (step) {
        var carSteps = [];
        for (var i = 0; i <= step; i++) {
            this.parking.prepare(this.commands);
            this.playingLength = this.parking.getCommandCount();
            this.parking.executePartially(i);
            this.hasError = this.hasError || this.parking.hasCommandError();
            var carX = this.parking.getCarPositionX();
            var carY = this.parking.getCarPositionY();
            var car = new CarStatus();
            car.id = 'car_' + i;
            car.y = (15 - carX) * 46;
            car.x = (carY - 1) * 46;
            var deg = this.parking.getCarRotation();
            car.transform = 'rotate(' + deg + 'deg)';
            car.backColor = '';
            // Short delay after the animation
            car.transition = this.animationSpeed * 0.7 / 1000.0;
            car.opacity = 0.3;
            if (i === step) {
                car.opacity = 1.0;
                car.id = 'last';
            }
            else if (i === 0) {
                car.backColor = 'black';
            }
            carSteps.push(car);
        }
        return carSteps;
    };
    AppComponent.prototype.commandsChanged = function () {
        try {
            this.hasError = false;
            this.playingStep = 0;
            this.cars = this.playNextStep(0);
            this.lastPosition = this.parking.getCarPositionString();
        }
        catch (err) {
            this.hasError = true;
        }
    };
    AppComponent.prototype.stepCommands = function () {
        try {
            this.playingStep++;
            if (this.playingStep >= this.playingLength) {
                if (this.timerSub) {
                    this.timerSub.unsubscribe();
                    this.timerSub = null;
                }
            }
            this.cars = this.playNextStep(this.playingStep);
            this.lastPosition = this.parking.getCarPositionString();
        }
        catch (err) {
            this.hasError = true;
        }
    };
    AppComponent.prototype.playCommands = function () {
        var _this = this;
        var period = this.animationSpeed;
        this.timerSub = __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["a" /* Observable */].timer(500, period).subscribe(function () {
            _this.stepCommands();
        });
    };
    AppComponent.prototype.stopCommands = function () {
        this.playingStep = 0;
        this.cars = this.playNextStep(0);
        this.lastPosition = this.parking.getCarPositionString();
        if (this.timerSub) {
            this.timerSub.unsubscribe();
            this.timerSub = null;
        }
    };
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: [__webpack_require__("../../../../../src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["E" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* AppComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
            ],
            providers: [],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "../../../../../src/app/auto-parking/implementation/car-position.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CarPosition; });
var CarPosition = /** @class */ (function () {
    function CarPosition(x, y) {
        this.valueX = x;
        this.valueY = y;
    }
    Object.defineProperty(CarPosition.prototype, "x", {
        get: function () {
            return this.valueX;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CarPosition.prototype, "y", {
        get: function () {
            return this.valueY;
        },
        enumerable: true,
        configurable: true
    });
    return CarPosition;
}());



/***/ }),

/***/ "../../../../../src/app/auto-parking/implementation/car-rotation.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CarRotation; });
var CarRotation = /** @class */ (function () {
    function CarRotation(direction, rotation) {
        this.directionValue = direction;
        this.rotationValue = rotation;
    }
    Object.defineProperty(CarRotation.prototype, "direction", {
        get: function () {
            return this.directionValue;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CarRotation.prototype, "rotation", {
        get: function () {
            return this.rotationValue;
        },
        enumerable: true,
        configurable: true
    });
    return CarRotation;
}());



/***/ }),

/***/ "../../../../../src/app/auto-parking/implementation/simple-parking-system.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SimpleParkingSystem; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__parking_system_inspect__ = __webpack_require__("../../../../../src/app/auto-parking/parking-system-inspect.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__car_position__ = __webpack_require__("../../../../../src/app/auto-parking/implementation/car-position.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__car_rotation__ = __webpack_require__("../../../../../src/app/auto-parking/implementation/car-rotation.ts");





var SimpleParkingSystem = /** @class */ (function () {
    function SimpleParkingSystem(width, height) {
        this.carPosition = new __WEBPACK_IMPORTED_MODULE_1__car_position__["a" /* CarPosition */](__WEBPACK_IMPORTED_MODULE_0__parking_system_inspect__["b" /* POSITION_INITIAL */], __WEBPACK_IMPORTED_MODULE_0__parking_system_inspect__["b" /* POSITION_INITIAL */]);
        this.carDirection = __WEBPACK_IMPORTED_MODULE_0__parking_system_inspect__["a" /* CarDirection */].North;
        this.carRotation = 0;
        /**
        * Indicates which command causes the error.
        */
        this.commandErrorPos = __WEBPACK_IMPORTED_MODULE_0__parking_system_inspect__["c" /* RESULT_SUCCESS */];
        this.commandActions = '';
        this.parkWidth = width;
        this.parkHeight = height;
    }
    SimpleParkingSystem.prototype.getCarPositionX = function () {
        return this.carPosition.x;
    };
    SimpleParkingSystem.prototype.getCarPositionY = function () {
        return this.carPosition.y;
    };
    SimpleParkingSystem.prototype.getCarDirection = function () {
        return this.carDirection;
    };
    SimpleParkingSystem.prototype.getCarRotation = function () {
        return this.carRotation;
    };
    SimpleParkingSystem.prototype.getCommandErrorPos = function () {
        return this.commandErrorPos;
    };
    SimpleParkingSystem.prototype.hasCommandError = function () {
        return this.commandErrorPos >= 0;
    };
    SimpleParkingSystem.prototype.getCarPositionString = function () {
        return this.carPosition.x + ',' + this.carPosition.y;
    };
    SimpleParkingSystem.prototype.getCommandCount = function () {
        return this.commandActions.length;
    };
    SimpleParkingSystem.prototype.checkIfExceeding = function (p) {
        if (p.x < 1 || p.x > this.parkWidth) {
            return true;
        }
        if (p.y < 1 || p.y > this.parkHeight) {
            return true;
        }
        return false;
    };
    SimpleParkingSystem.prototype.setFirstErrorPos = function (pos) {
        if (this.commandErrorPos === __WEBPACK_IMPORTED_MODULE_0__parking_system_inspect__["c" /* RESULT_SUCCESS */]) {
            this.commandErrorPos = pos;
        }
    };
    SimpleParkingSystem.prototype.prepare = function (input) {
        this.commandErrorPos = __WEBPACK_IMPORTED_MODULE_0__parking_system_inspect__["c" /* RESULT_SUCCESS */];
        var positionString = '';
        var instructions = '';
        var parts = input.split(':', 2);
        if (parts.length === 2) {
            positionString = parts[0];
            instructions = parts[1];
        }
        else {
            throw new Error('Wrong commands');
        }
        var initialX = __WEBPACK_IMPORTED_MODULE_0__parking_system_inspect__["b" /* POSITION_INITIAL */];
        var initialY = __WEBPACK_IMPORTED_MODULE_0__parking_system_inspect__["b" /* POSITION_INITIAL */];
        var positionParts = positionString.split(',', 2);
        if (positionParts.length === 2) {
            initialX = Number.parseInt(positionParts[0]);
            initialY = Number.parseInt(positionParts[1]);
        }
        else {
            throw new Error('Wrong initial position');
        }
        this.carPosition = new __WEBPACK_IMPORTED_MODULE_1__car_position__["a" /* CarPosition */](initialX, initialY);
        this.carDirection = __WEBPACK_IMPORTED_MODULE_0__parking_system_inspect__["a" /* CarDirection */].North;
        this.commandActions = instructions;
        this.carRotation = 0;
        if (this.checkIfExceeding(this.carPosition)) {
            this.setFirstErrorPos(0);
            throw new Error('Wrong initial position');
        }
    };
    SimpleParkingSystem.prototype.turnLeft = function (r) {
        var newDirection = r.direction;
        var newRotation = r.rotation - 90;
        switch (r.direction) {
            case __WEBPACK_IMPORTED_MODULE_0__parking_system_inspect__["a" /* CarDirection */].North:
                newDirection = __WEBPACK_IMPORTED_MODULE_0__parking_system_inspect__["a" /* CarDirection */].West;
                break;
            case __WEBPACK_IMPORTED_MODULE_0__parking_system_inspect__["a" /* CarDirection */].West:
                newDirection = __WEBPACK_IMPORTED_MODULE_0__parking_system_inspect__["a" /* CarDirection */].South;
                break;
            case __WEBPACK_IMPORTED_MODULE_0__parking_system_inspect__["a" /* CarDirection */].South:
                newDirection = __WEBPACK_IMPORTED_MODULE_0__parking_system_inspect__["a" /* CarDirection */].East;
                break;
            case __WEBPACK_IMPORTED_MODULE_0__parking_system_inspect__["a" /* CarDirection */].East:
                newDirection = __WEBPACK_IMPORTED_MODULE_0__parking_system_inspect__["a" /* CarDirection */].North;
                break;
            default:
                throw new Error('Wrong direction');
        }
        return new __WEBPACK_IMPORTED_MODULE_2__car_rotation__["a" /* CarRotation */](newDirection, newRotation);
    };
    SimpleParkingSystem.prototype.turnRight = function (r) {
        var newDirection = r.direction;
        var newRotation = r.rotation + 90;
        switch (r.direction) {
            case __WEBPACK_IMPORTED_MODULE_0__parking_system_inspect__["a" /* CarDirection */].North:
                newDirection = __WEBPACK_IMPORTED_MODULE_0__parking_system_inspect__["a" /* CarDirection */].East;
                break;
            case __WEBPACK_IMPORTED_MODULE_0__parking_system_inspect__["a" /* CarDirection */].West:
                newDirection = __WEBPACK_IMPORTED_MODULE_0__parking_system_inspect__["a" /* CarDirection */].North;
                break;
            case __WEBPACK_IMPORTED_MODULE_0__parking_system_inspect__["a" /* CarDirection */].South:
                newDirection = __WEBPACK_IMPORTED_MODULE_0__parking_system_inspect__["a" /* CarDirection */].West;
                break;
            case __WEBPACK_IMPORTED_MODULE_0__parking_system_inspect__["a" /* CarDirection */].East:
                newDirection = __WEBPACK_IMPORTED_MODULE_0__parking_system_inspect__["a" /* CarDirection */].South;
                break;
            default:
                throw new Error('Wrong direction');
        }
        return new __WEBPACK_IMPORTED_MODULE_2__car_rotation__["a" /* CarRotation */](newDirection, newRotation);
    };
    SimpleParkingSystem.prototype.moveForword = function (direction, location) {
        var x = location.x;
        var y = location.y;
        switch (direction) {
            case __WEBPACK_IMPORTED_MODULE_0__parking_system_inspect__["a" /* CarDirection */].North:
                x++;
                break;
            case __WEBPACK_IMPORTED_MODULE_0__parking_system_inspect__["a" /* CarDirection */].West:
                y--;
                break;
            case __WEBPACK_IMPORTED_MODULE_0__parking_system_inspect__["a" /* CarDirection */].South:
                x--;
                break;
            case __WEBPACK_IMPORTED_MODULE_0__parking_system_inspect__["a" /* CarDirection */].East:
                y++;
                break;
            default:
                throw new Error('Wrong direction');
        }
        return new __WEBPACK_IMPORTED_MODULE_1__car_position__["a" /* CarPosition */](x, y);
    };
    SimpleParkingSystem.prototype.executePartially = function (count) {
        var commandCountToRun = Math.min(count, this.commandActions.length);
        for (var i = 0; i < commandCountToRun; i++) {
            var newPosition = this.carPosition;
            var rotation = new __WEBPACK_IMPORTED_MODULE_2__car_rotation__["a" /* CarRotation */](this.carDirection, this.carRotation);
            var command = this.commandActions.charAt(i);
            switch (command) {
                case 'L':
                    rotation = this.turnLeft(rotation);
                    this.carDirection = rotation.direction;
                    this.carRotation = rotation.rotation;
                    break;
                case 'R':
                    rotation = this.turnRight(rotation);
                    this.carDirection = rotation.direction;
                    this.carRotation = rotation.rotation;
                    break;
                case 'F':
                    newPosition = this.moveForword(this.carDirection, newPosition);
                    break;
                default:
                    this.setFirstErrorPos(i + 1);
                    throw new Error('Wrong command');
            }
            if (this.checkIfExceeding(newPosition)) {
                // Only save the first error position, ignore the command
                this.setFirstErrorPos(i + 1);
            }
            else {
                this.carPosition = newPosition;
            }
        }
    };
    SimpleParkingSystem.prototype.execute = function (commands) {
        this.prepare(commands);
        this.executePartially(this.getCommandCount());
    };
    return SimpleParkingSystem;
}());



/***/ }),

/***/ "../../../../../src/app/auto-parking/parking-system-inspect.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CarDirection; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return RESULT_SUCCESS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return POSITION_INITIAL; });
var CarDirection;
(function (CarDirection) {
    CarDirection[CarDirection["North"] = 1] = "North";
    CarDirection[CarDirection["South"] = 2] = "South";
    CarDirection[CarDirection["West"] = 3] = "West";
    CarDirection[CarDirection["East"] = 4] = "East";
})(CarDirection || (CarDirection = {}));
var RESULT_SUCCESS = -1;
var POSITION_INITIAL = -1;


/***/ }),

/***/ "../../../../../src/assets/taxi.png":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "taxi.f8e1d447aa3749b2111b.png";

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_7" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map